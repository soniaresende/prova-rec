package com.example.recuperacaoSonia.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.recuperacaoSonia.R

class MainActivity : AppCompatActivity() {

    private lateinit var botaoAdicionarVeiculo : Button
    private lateinit var botaoVerVeiculos : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        botaoAdicionarVeiculo = findViewById(R.id.btnAdicionarVeiculo)
        botaoVerVeiculos = findViewById(R.id.btnVerVeiculos)
    }

    fun onClickAdicionarVeiculo(v: View){
        val intent = Intent(this, ActivityAddVeiculo::class.java)
        startActivity(intent)
    }

    fun onClickVerVeiculos(v: View){
        val intent = Intent(this, ActivityListVeiculo::class.java)
        startActivity(intent)
    }
}