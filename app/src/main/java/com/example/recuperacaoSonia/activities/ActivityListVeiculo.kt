package com.example.recuperacaoSonia.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recuperacaoSonia.VeiculoListaAdapter
import com.example.recuperacaoSonia.R
import com.example.recuperacaoSonia.data.DBTableManager

class ActivityListVeiculo : AppCompatActivity() {
    private lateinit var rvCarros : RecyclerView
    private lateinit var dbCarros : DBTableManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_veiculo)

        this.rvCarros = findViewById(R.id.rvCarros)
        this.dbCarros = DBTableManager(this)
        this.rvCarros.layoutManager = LinearLayoutManager(this)
        this.rvCarros.adapter = VeiculoListaAdapter(dbCarros.receberCarros())
    }
}