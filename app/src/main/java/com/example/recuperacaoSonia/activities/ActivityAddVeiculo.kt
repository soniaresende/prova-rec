package com.example.recuperacaoSonia.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.recuperacaoSonia.R
import com.example.recuperacaoSonia.data.DBTableManager
import com.example.recuperacaoSonia.model.Veiculo
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class ActivityAddVeiculo : AppCompatActivity() {

    private lateinit var spinner : Spinner
    private lateinit var valMinEntrada : EditText
    private lateinit var dataEntradaEntrada : EditText
    private lateinit var descricaoEntrada : EditText
    private lateinit var hojeBotao : Button
    private lateinit var addVeiculoBotao : Button
    private lateinit var veiculoDB : DBTableManager

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_veiculo)

        this.spinner = findViewById(R.id.spinner)
        val adapter = ArrayAdapter.createFromResource(this, R.array.arrayCategorias,android.R.layout.simple_spinner_item)
            .also{adapter -> adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)}
        this.spinner.adapter = adapter

        //--------------------------Inicialização-----------------------

        this.dataEntradaEntrada = findViewById(R.id.caixaDataEntrada)
        this.valMinEntrada = findViewById(R.id.caixaValorMinimo)
        this.addVeiculoBotao = findViewById(R.id.botaoAdicionarVeiculo)
        this.descricaoEntrada = findViewById(R.id.caixaDescricao)
        this.hojeBotao = findViewById(R.id.botaoHoje)
        this.veiculoDB = DBTableManager(this)
    }

    /*
        onClick no botão hoje
    */

    fun onClickHoje(v: View){
        val current = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        val formatted = current.format(formatter)

        this.dataEntradaEntrada.setText(formatted)

    }

    fun onClickAdicionar(v: View){

        var verificar : Boolean = true

        val categoria = this.spinner.selectedItem.toString()
        val valMin = this.valMinEntrada.text.toString()
        val dataEntrada = this.dataEntradaEntrada.text.toString()
        val descricao = this.descricaoEntrada.text.toString()

        if(valMin.isEmpty()){
            verificar = false
        }
        if(dataEntrada.isEmpty()){
            verificar = false
        }
        if(descricao.isEmpty()){
            verificar = false
        }

        if(verificar){
            val car = Veiculo(categoria, valMin.toDouble(),dataEntrada,descricao)
            veiculoDB.inserir(car)

            Toast.makeText(this, R.string.veiculo_adicionado,Toast.LENGTH_SHORT).show()
            finish()
        }   else{
            Toast.makeText(this, R.string.erro_adicionar,Toast.LENGTH_SHORT).show()
        }

    }
}