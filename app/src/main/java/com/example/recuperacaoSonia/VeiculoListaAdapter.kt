package com.example.recuperacaoSonia

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recuperacaoSonia.model.Veiculo

class VeiculoListaAdapter(private val listaVeiculos : ArrayList<Veiculo>) : RecyclerView.Adapter<VeiculoListaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VeiculoListaViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.view_veiculo, parent, false)
        return VeiculoListaViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: VeiculoListaViewHolder, position: Int) {
        holder.bind(this.listaVeiculos[position])
    }

    override fun getItemCount(): Int {
        return this.listaVeiculos.size
    }
}