package com.example.recuperacaoSonia.model

class Veiculo(private val categoria: String, private val valMin: Double, private val dataEntrada: String, private val descricao: String) {
    private var id : Long = 0

    constructor(id : Long, categoria: String,valMin: Double,dataEntrada: String,descricao: String ): this(categoria, valMin, dataEntrada, descricao){
        this.id = id
    }

    fun getCategoria(): String{
        return categoria
    }

    fun getValMin(): Double{
        return valMin
    }

    fun getDataEntrada(): String{
        return dataEntrada
    }

    fun getDescricao(): String{
        return descricao
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Veiculo

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }


}