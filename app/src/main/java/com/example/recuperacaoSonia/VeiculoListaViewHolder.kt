package com.example.recuperacaoSonia

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.recuperacaoSonia.model.Veiculo

class VeiculoListaViewHolder (
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val textoCategoria: TextView = itemView.findViewById(R.id.textoCategoria)
    private val textoValMin: TextView = itemView.findViewById(R.id.textoValMin)
    private val textoDataEntrada: TextView = itemView.findViewById(R.id.textoDataEntrada)
    private val textoDescricao: TextView = itemView.findViewById(R.id.textoDescricao)
    private val txtDescricao: TextView = itemView.findViewById(R.id.txtDescricao)
    private lateinit var veiculo: Veiculo

    init{
        this.txtDescricao.setOnClickListener{
            if(textoDescricao.visibility == View.GONE){
                textoDescricao.visibility = View.VISIBLE
                txtDescricao.setText(R.string.descricao_retrair)
            }   else{
                textoDescricao.visibility = View.GONE
                txtDescricao.setText(R.string.descricao_expandir)
            }
        }
    }

    fun bind(veiculo: Veiculo){
        this.veiculo = veiculo
        this.textoCategoria.text = veiculo.getCategoria()
        this.textoValMin.text = "$" + veiculo.getValMin().toString()
        this.textoDataEntrada.text = veiculo.getDataEntrada()
        this.textoDescricao.text = veiculo.getDescricao()

    }
}