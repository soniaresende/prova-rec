package com.example.recuperacaoSonia.data

object DBSchema {
    object Table{
        const val TABLENAME = "veiculos"
        const val ID = "v_id"
        const val CATEGORIA = "v_categoria"
        const val VAL_MIN = "v_valor_min"
        const val DATA_ENTRADA = "v_data_entrada"
        const val DESCRICAO = "v_descricao"

        fun getCreateTableQuery(): String{
            return """ 
                CREATE TABLE IF NOT EXISTS $TABLENAME (
                    $ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    $CATEGORIA TEXT NOT NULL,
                    $VAL_MIN REAL NOT NULL,
                    $DATA_ENTRADA TEXT NOT NULL,
                    $DESCRICAO TEXT NOT NULL
                );
            """.trimIndent()
        }
    }
}