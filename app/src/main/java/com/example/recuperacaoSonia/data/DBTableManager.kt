package com.example.recuperacaoSonia.data

import android.content.ContentValues
import android.content.Context
import com.example.recuperacaoSonia.model.Veiculo

class DBTableManager(context : Context) {
    companion object{
        val COLS = arrayOf(
            DBSchema.Table.ID,
            DBSchema.Table.CATEGORIA,
            DBSchema.Table.VAL_MIN,
            DBSchema.Table.DATA_ENTRADA,
            DBSchema.Table.DESCRICAO
        )
    }

    private val dbHelper = DBHelper(context)

    fun inserir(veiculo: Veiculo) {
        val cv = ContentValues()
        cv.put(DBSchema.Table.CATEGORIA, veiculo.getCategoria())
        cv.put(DBSchema.Table.VAL_MIN, veiculo.getValMin())
        cv.put(DBSchema.Table.DATA_ENTRADA, veiculo.getDataEntrada())
        cv.put(DBSchema.Table.DESCRICAO, veiculo.getDescricao())
        val db = this.dbHelper.writableDatabase
        db.insert(DBSchema.Table.TABLENAME, null, cv)
        db.close()
    }

    fun receberCarros(): ArrayList<Veiculo> {
        val veiculos : ArrayList<Veiculo> = ArrayList()
        val db = this.dbHelper.readableDatabase
        val cur = db.query(DBSchema.Table.TABLENAME, COLS, null, null, null, null, null)
        while(cur.moveToNext()){
            val id = cur.getLong(cur.getColumnIndexOrThrow(DBSchema.Table.ID))
            val category = cur.getString(cur.getColumnIndexOrThrow(DBSchema.Table.CATEGORIA))
            val minValue = cur.getDouble(cur.getColumnIndexOrThrow(DBSchema.Table.VAL_MIN))
            val entryDate = cur.getString(cur.getColumnIndexOrThrow(DBSchema.Table.DATA_ENTRADA))
            val description = cur.getString(cur.getColumnIndexOrThrow(DBSchema.Table.DESCRICAO))
            val carro = Veiculo(id,category,minValue,entryDate,description)
            veiculos.add(carro)
        }
        db.close()
        return veiculos
    }
}